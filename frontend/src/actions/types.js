export const GET_VIDEOS = 'GET_VIDEOS'
export const GET_VIDEO = 'GET_VIDEO'
export const CREATE_VIDEO = 'CREATE_VIDEO'
export const UPDATE_VIDEO = 'UPDATE_VIDEO'
export const DELETE_VIDEO = 'DELETE_VIDEO'

export const GET_STATUS = 'GET_STATUS'

export const ADD_ERROR = 'ADD_ERROR'
export const REMOVE_ERROR = 'REMOVE_ERROR'
